<?php
namespace Tests;

abstract class TestAbstract
    extends \PHPUnit_Framework_TestCase
{
    const PROJECT_ROOT = __dir__ . '/../../';

    protected $projectRoot = self::PROJECT_ROOT;

}
