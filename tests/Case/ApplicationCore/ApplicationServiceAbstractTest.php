<?php
/**
 * 抽象アプリケーションサービス ユニットテスト
 */
namespace Tests\ApplicationCore;

use Tests\ApplicationCore\ApplicationServiceAbstract as TestObject;

/**
 * @coversDefaultClass ApplicationCore\ApplicationServiceAbstract
 */
final class ApplicationServiceAbstractTest
    extends \PHPUnit_Framework_TestCase
{

    /**
     * @covers ::createParameter
     */
    public function test_createParameter ()
    {
        /* Arrange */
        $service = TestObject\TestService::instance();
        /* Act */
        $parameter = $service->createParameter();
        /* Assert */
        $this->assertInstanceOf(TestObject\TestParameter::class, $parameter);
    }

    public function createViewModelDataProvider ()
    {
        yield '#0' => ['parameters' => ['id' => 'testId', 'password' => 'test_password']];
        yield '#1' => ['parameters' => null];
    }

    /**
     * @dataProvider createViewModelDataProvider
     * @covers ::createResult
     */
    public function test_createViewModel ($parameters)
    {
        /* Arrange */
        $service = TestObject\TestService::instance();
        $parameter = $service->createParameter();
        if (!is_null($parameters)) {
            $parameter->setId($parameters['id'])
                ->setPassword($parameters['password']);
        }
        /* Act */
        $result = $service->createResult($parameter);
        /* Assert */
        $this->assertInstanceOf(TestObject\TestResult::class, $result);
        $this->assertEquals($parameter, $result->parameter);
    }

}

namespace Tests\ApplicationCore\ApplicationServiceAbstract;

class TestParameter
    extends \ApplicationCore\ServiceParameterAbstract
{
    private $id;
    private $password;
}

class TestResult
    extends \ApplicationCore\ServiceResultAbstract
{
}

class TestService
    extends \ApplicationCore\ApplicationServiceAbstract
{
}
