<?php
/**
 * ドメイン通知ハンドラ ユニットテスト
 */
namespace Tests\ApplicationCore;

use ApplicationCore;
use DomainCore\Model as DomainCore;

/**
 * @coversDefaultClass ApplicationCore\NotificationHandler
 */
final class NotificationHandlerTest
    extends \Tests\TestAbstract
{

    /**
     * @group categories
     * @covers ::notify
     * @covers ::categories
     */
    public function test_notify ()
    {
        /* Arrange */
        $handler = $this->sut();
        $handler
            ->notify('category1', DomainCore\Notification::error('category1-error1'))
            ->notify('category1', DomainCore\Notification::error('category1-error2'))
            ->notify('category1', DomainCore\Notification::warning('category1-warning1'))
            ->notify('category2', DomainCore\Notification::notice('category2-notice1'))
            ->notify('category2', DomainCore\Notification::warning('category2-warning1'))
            ->notify('category3', DomainCore\Notification::notice('category3-notice1'))
            ->notify('category4', DomainCore\Notification::warning('category4-warning1'));
        /* Act */
        $categories = $handler->categories();
        /* Assert */
        $this->assertEquals(
            ['category1', 'category2', 'category3', 'category4',], $categories);
        return $handler;
    }

    /**
     * @depends test_notify
     * @group hasCategory
     * @covers ::hasCategory
     */
    public function test_hasCategory ($handler)
    {
        /* Arrange */
        /* Act */
        /* Assert */
        $this->assertTrue($handler->hasCategory('category1'));
        $this->assertTrue($handler->hasCategory('category2'));
        $this->assertTrue($handler->hasCategory('category3'));
        $this->assertTrue($handler->hasCategory('category4'));
        $this->assertFalse($handler->hasCategory('category5'));
    }

    /**
     * @depends test_notify
     * @group hasType
     * @covers ::hasType
     */
    public function test_hasType ($handler)
    {
        /* Arrange */
        /* Act */
        /* Assert */
        $this->assertTrue($handler->hasType('error'));
        $this->assertTrue($handler->hasType('Error'));
        $this->assertTrue($handler->hasType('warning'));
        $this->assertTrue($handler->hasType('Warning'));
        $this->assertTrue($handler->hasType('notice'));
        $this->assertTrue($handler->hasType('Notice'));
        $this->assertFalse($handler->hasType('debug'));
        /* カテゴライズされた通知の検証 */
        $this->assertTrue($handler->hasType('error', 'category1'));
        $this->assertTrue($handler->hasType('warning', 'category1'));
        $this->assertTrue($handler->hasType('warning', 'category2'));
        $this->assertTrue($handler->hasType('notice', 'category2'));
        $this->assertTrue($handler->hasType('notice', 'category3'));
        $this->assertTrue($handler->hasType('warning', 'category4'));
    }

    /**
     * @depends test_notify
     * @group notifications
     * @covers ::notifications
     */
    public function test_notifications ($handler)
    {
        /* Arrange */
        /* Act */
        /* Assert */
        $this->assertEquals([
            DomainCore\Notification::notice('category2-notice1'),
            DomainCore\Notification::warning('category2-warning1')
        ], $handler->notifications('category2'));
        $this->assertEquals([
            DomainCore\Notification::error('category1-error1'),
            DomainCore\Notification::error('category1-error2')
        ], $handler->notifications('category1', ['error']));
    }

    /**
     * @depends test_notify
     * @group hasTypeOnCatgory
     * @covers ::hasTypeOnCatgory
     */
    public function test_hasTypeOnCatgory ($handler)
    {
        /* Arrange */
        $closure = \Closure::bind(
            function ($type, $category) {
                return $this->hasTypeOnCatgory($type, $category);
            }, $handler, get_class($handler)
        );
        /* Act */
        /* Assert */
        $this->assertTrue($closure('notice', 'category2'));
        $this->assertFalse($closure('error', 'category2'));
        $this->assertFalse($closure('error', 'category5'));
    }

    private function sut ()
    {
        return ApplicationCore\NotificationHandler::instance();
    }

}
