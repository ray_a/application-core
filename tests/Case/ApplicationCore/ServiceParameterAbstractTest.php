<?php
/**
 * 抽象サービスパラメータ ユニットテスト
 */
namespace Tests\ApplicationCore;

use Tests\ApplicationCore\ServiceParameterAbstract as TestObject;

/**
 * @coversDefaultClass \Applicationcore\ServiceParameterAbstract
 */
class ServiceParameterAbstractTest
    extends \Tests\TestAbstract
{

    public function parseDataProvider ()
    {
        yield '#0' => [
            'keyValues' => [
                'id' => 5, 'first_name' => '名', 'last_name' => '姓',
                'birth_of_date' => '1980-01-23 00:00:00',
            ],
            'mappingClosure' => function ($field) {
                return strtolower(preg_replace('/([A-Z])/u', '_$1', $field));
            },
            'expect' => [
                'id' => 5, 'firstName' => '名', 'lastName' => '姓',
                'birthOfDate' => '1980-01-23 00:00:00',

            ],
        ];
        yield '#1' => [
            'keyValues' => [
                'id' => 5, 'firstName' => '名', 'lastName' => '姓',
                'birthOfDate' => '1980-01-23 00:00:00',
            ],
            'mappingClosure' => null,
            'expect' => [
                'id' => 5, 'firstName' => '名', 'lastName' => '姓',
                'birthOfDate' => '1980-01-23 00:00:00',

            ],
        ];
    }

    /**
     * @dataProvider parseDataProvider
     * @group parse
     * @covers ::parse
     */
    public function test_parse ($keyValues, $mappingClosure, $expect)
    {
        /* Arrange */
        $parameter = TestObject\TestParameter::instance();
        /* Act */
        $parameter->parse($keyValues, $mappingClosure);
        /* Assert */
        foreach ($expect as $property => $value) {
            $this->assertEquals($value, $parameter->{$property});
        }
    }

    public function getValueSafetyDataProvider ()
    {
        $keyValues = [
            'id' => 5, 'first_name' => '名', 'last_name' => '姓',
            'birth_of_date' => '1980-01-23 00:00:00',
        ];
        yield '#0' => [
            'keyValues' => $keyValues, 'key' => 'id', 'default' => null, 'expect' => 5];
    }

    /**
     * @dataProvider getValueSafetyDataProvider
     * @group getValueSafety
     * @covers ::getValueSafety
     */
    public function test_getValueSafety ($keyValues, $key, $default, $expect)
    {
        /* Arrange */
        $parameter = TestObject\TestParameter::instance();
        /* Act */
        $result = \Closure::bind(
            function ($keyValues, $key, $default) {
                return $this->getValueSafety($keyValues, $key, $default);
            }, $parameter, get_class($parameter)
        )->__invoke($keyValues, $key, $default);
        /* Assert */
        $this->assertEquals($expect, $result);
    }

}

namespace Tests\ApplicationCore\ServiceParameterAbstract;

class TestParameter
    extends \ApplicationCore\ServiceParameterAbstract
{
    private $id;
    private $firstName;
    private $lastName;
    private $birthOfDate;
}
