<?php
/**
 * 抽象データ転送オブジェクト ユニットテスト
 */
namespace Tests\ApplicationCore;

use Tests\ApplicationCore\DtoAbstract\TestDto;

/**
 * @coversDefaultClass ApplicationCore\DtoAbstract
 */
final class DtoAbstractTest
    extends \PHPUnit_Framework_TestCase
{

    /**
     * @covers ::__call
     */
    public function test_setAndGet ()
    {
        /* Arrange */
        $id = 1;
        $name = 'test name';
        $email = 'test@test.jp';
        /* Act */
        $dto = TestDto::instance()
            ->setId($id)
            ->setName($name)
            ->setEmail($email);
        /* Assert */
        $this->assertEquals($id, $dto->id);
        $this->assertEquals($name, $dto->name);
        $this->assertEquals($email, $dto->email);
    }

    /**
     * @covers ::__call
     * @covers ::createUndifinedMethodException
     */
    public function test_invokeSetterForNonExistingFieldThrowsException ()
    {
        /* Arrange */
        /* Act */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessageRegExp(
            '/^Undefined method '
            . 'Tests\\\ApplicationCore\\\DtoAbstract\\\TestDto::setPassword '
            . 'via __call\(\)$/');
        $dto = TestDto::instance()
            ->setPassword('testPassword');
        /* Assert */
    }

    /**
     * @covers ::__call
     * @covers ::createUndifinedMethodException
     */
    public function test_invokeNonExistingMethodThrowsException ()
    {
        /* Arrange */
        /* Act */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessageRegExp(
            '/^Undefined method '
            . 'Tests\\\ApplicationCore\\\DtoAbstract\\\TestDto::changePassword '
            . 'via __call\(\)$/');
        $dto = TestDto::instance()
            ->changePassword('changePassword');
        /* Assert */
    }


}

namespace Tests\ApplicationCore\DtoAbstract;

use ApplicationCore as Core;

class TestDto
    extends Core\DtoAbstract
{
    private $id;
    private $name;
    private $email;
}
