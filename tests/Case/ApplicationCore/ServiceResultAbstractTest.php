<?php
/**
 * 抽象サービス結果 ユニットテスト
 */
namespace Tests\ApplicationCore;

use Tests\ApplicationCore\ServiceResultAbstract as TestObject;

/**
 * @coversDefaultClass \ApplicationCore\ServiceResultAbstract
 */
class ServiceResultAbstractTest
    extends \Tests\TestAbstract
{

    /**
     * @group instantiate
     * @covers ::__construct
     */
    public function test_instantiate ()
    {
        /* Arrange */
        $parameter = TestObject\TestServiceParameter::instance();
        /* Act */
        $result = TestObject\TestServiceResult::instance($parameter);
        /* Assert */
        $this->assertInstanceOf(
            \ApplicationCore\NotificationHandler::class, $result->notificationHandler);
        $this->assertEquals($parameter, $result->parameter);
    }

}

namespace Tests\ApplicationCore\ServiceResultAbstract;

class TestServiceParameter
    extends \ApplicationCore\ServiceParameterAbstract
{
}

class TestServiceResult
    extends \ApplicationCore\ServiceResultAbstract
{
}
