<?php
/**
 * ドメイン通知ハンドラ
 */
namespace ApplicationCore;

use DomainCore\Model as DomainCore;

class NotificationHandler
    implements DomainCore\INotificationHandler
{

    /** Traits */
    use \PhpTypeExtension\Traits\StaticInstantiatable;

    /** ドメイン通知リスト @var array<string, DomainCore\Notification[]> */
    private $notifications = [];

    /**
     * @inheritdoc
     */
    public function categories ()
    {
        return array_keys($this->notifications);
    }

    /**
     * @inheritdoc
     */
    public function hasCategory ($category)
    {
        return array_key_exists($category, $this->notifications);
    }

    /**
     * @inheritdoc
     */
    public function notify ($category, DomainCore\Notification $notification)
    {
        if (!array_key_exists($category, $this->notifications)) {
            $this->notifications[$category] = [];
        }
        $this->notifications[$category][] = $notification;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function hasType ($type, $category = null)
    {
        if (!is_null($category)) {
            return $this->hasTypeOnCatgory($type, $category);
        }
        foreach ($this->categories() as $category) {
            if ($this->hasTypeOnCatgory($type, $category)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function notifications ($category, array $filter = [])
    {
        if (!$this->hasCategory($category)) return [];
        $filter = array_map(function ($type) {return strtolower($type);}, $filter);
        return array_reduce(
            $this->notifications[$category], function ($carry, $notification) use ($filter) {
                if (!empty($filter) && !in_array($notification->type, $filter)) {
                    return $carry;
                }
                return array_merge($carry, [$notification]);
            }, []
        );
    }

    /**
     * @param string $type
     * @param string $category
     * @return boolean
     */
    private function hasTypeOnCatgory ($type, $category)
    {
        if (!$this->hasCategory($category)) return false;
        return array_reduce(
            $this->notifications[$category], function ($carry, $notification) use ($type) {
                if ($notification->isType($type)) $carry = true;
                return $carry;
            }, false);
    }

}
