<?php
/**
 * 抽象サービスパラメータ
 */
namespace ApplicationCore;

abstract class ServiceParameterAbstract
    extends DtoAbstract
{

    /**
     * キーバリュー形式の配列をフィールドに展開する
     * @param array<string, mixed> $keyValues キーバリュー配列
     * @param callable $mappingClosure OPTIONAL フィールド名を受けて配列キーを返すクロージャ
     * @return self
     */
    public function parse (array $keyValues, callable $mappingClosure = null)
    {
        $mappingClosure = is_null($mappingClosure)
            ? function ($field) {return $field;}
            : $mappingClosure; 
        return \Closure::bind(
            function ($keyValues, $mappingClosure) {
                foreach (array_keys(get_object_vars($this)) as $field) {
                    $key = $mappingClosure($field);
                    $this->{$field} = $this->getValueSafety($keyValues, $key);
                }
                return $this;
            }, $this, static::class
        )->__invoke($keyValues, $mappingClosure);
         
    }

    /**
     * キーバリューからキーとキーが存在しない場合の既定値を指定して値を取得する
     * @param array<string, mixed> $keyValues キーバリュー配列
     * @param string $key キー
     * @param mixed $default OPTIONAL キーが存在しない場合の既定値
     * @return mixed
     */
    protected function getValueSafety ($keyValues, $key, $default = null)
    {
        return array_key_exists($key, $keyValues) ? $keyValues[$key] : $default;
    }

}
