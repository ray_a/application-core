<?php
/**
 * 抽象データ転送オブジェクト
 */
namespace ApplicationCore;

use PhpTypeExtension\Traits;

abstract class DtoAbstract
{

    /** Traits */
    use
        /* 静的ファクトリトレイト  */
        Traits\StaticInstantiatable,
        /* 読み取り専用プロパティトレイト */
        Traits\ReadOnlyFieldsDefinitionable;

    /**
     * 汎用セッターメソッドサポート
     * @param string $name メソッド名
     * @param mixed[] $args OPTIONAL 引数リスト
     * @return mixed
     */
    public function __call ($name, array $args = [])
    {
        if (preg_match('/^set/u', $name) != 0) {
            $value = reset($args);
            return \Closure::bind(
                function ($name, $value) {
                    $field = lcfirst(preg_replace('/^set/u', '', $name));
                    if (in_array($field, array_keys(get_object_vars($this)))) {
                        $this->{$field} = $value;
                        return $this;
                    }
                    throw $this->createUndifinedMethodException($name);
                }, $this, static::class
            )->__invoke($name, $value);
        }
        throw $this->createUndifinedMethodException($name);
    }

    /**
     * メソッド未定義例外を生成する
     * @param string $methodName メソッド名
     * @return \RuntimeException
     */
    protected function createUndifinedMethodException ($methodName)
    {
        $className = static::class;
        return new \RuntimeException(
            "Undefined method {$className}::{$methodName} via __call()");
    }

}
