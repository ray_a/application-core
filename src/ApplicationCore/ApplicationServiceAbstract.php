<?php
/**
 * 抽象アプリケーションサービス
 */
namespace ApplicationCore;

use PhpTypeExtension\Traits;

abstract class ApplicationServiceAbstract
{

    /** Traits */
    use Traits\StaticInstantiatable;

    /**
     * サービスパラメータを生成する
     * @return ServiceParameterAbstract
     */
    public function createParameter ()
    {
        $parameterName = preg_replace('/Service$/u', 'Parameter', static::class);
        return $parameterName::instance();
    }

    /**
     * サービス結果を生成する
     * @param ServiceParameterAbstract $parameter OPTIONAL サービスパラメータ
     * @return ServiceResultAbstract
     */
    public function createResult (ServiceParameterAbstract $parameter = null)
    {
        $parameter = is_null($parameter) ? $this->createParameter() : $parameter;
        $resultName = preg_replace('/Service$/u', 'Result', static::class);
        return $resultName::instance($parameter);
    }

}
