<?php
/**
 * 抽象サービス結果
 */
namespace ApplicationCore;

use DomainCore\Model as DomainCore;

abstract class ServiceResultAbstract
    extends DtoAbstract
{

    /** 通知ハンドラ @var NotificationHandler */
    protected $notificationHandler;

    /** サービス実行パラメータ @var ServicePrameterAbstract */
    protected $parameter;

    protected function __construct (
        ServiceParameterAbstract $parameter)
    {
        $this->parameter = $parameter;
        $this->notificationHandler = NotificationHandler::instance();
    }

}
